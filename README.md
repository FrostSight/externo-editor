# Externo Editor

Externo Editor is a project that I built in my 3rd semester. This project was developed in a Java environment. The framework, Javafx was used to develop Externo Editor. Additionally, software, scene builder was needed to design the overall outlook including the user interface.

Externo Editor is a text editor that allows users to create, edit and save text files in different formats. It also has the ability to open and view pdf files within the application. Externo Editor has a simple and user-friendly interface that makes it easy to use


## Features

- Notepad
- Text (file) converter
- Scrollbar visible/invisible mode toogle
- Pdf opener
- Multiple theme
- Cross platform


## Tech Used

**Language** Java, CSS

**Framework** JavaFx

**Software** Intellij Idea, Scene Builder


## Feedback

If you have any feedback, please reach out to at http://scr.im/84ta


## Contributing

Contributions are always welcome!

Please adhere to this project's `code of conduct`.

